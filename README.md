# Modern DevOps Infrastructure

Prerequisites:

- [Vagrant](https://www.vagrantup.com/)
- [VirtualBox](https://www.virtualbox.org/)

Get started:

```
vagrant up
```

Installed software:

- Portainer: http://portainer.localtest.me
- Gitlab: http://gitlab.localtest.me (user: root)

Get token: http://gitlab.localtest.me/admin/runners

```
vagrant ssh

docker exec -it gitlab-runner \
  gitlab-runner register \
    --non-interactive \
    --registration-token TOKEN \
    --locked=false \
    --description docker-stable \
    --url http://gitlab:8099 \
    --executor docker \
    --docker-image docker:stable \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --docker-network-mode vagrant_gitlab \
    --clone-url=http://gitlab:8099
```

Import project from https://gitlab.com/erikhofer/devops-guestbook
